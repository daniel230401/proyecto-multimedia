#include <SFML/Graphics.hpp>
#include "Prueba2/MainMenu.h"
#include "Prueba2/Sonido.h"
#include <time.h>
#include <list>
#include <iostream>
#include <SFML/Audio.hpp>//para los sonidos
#include "Prueba2/Musica.h"
#include <ctime>
#include "Prueba2/GameOver.h"

using namespace sf;

const int W = 1600; //dimensiones pantalla
const int H = 900;
bool coheteUsado = false;
bool balaUsada = false;
float DEGTORAD = 0.017453f; //convertir de grados a radianes

const int NUM_ASTEROIDES_INICIALES = 4;
const int TASA_GENERACION_ASTEROIDES = 350; //cuanto mas alto menos se generan
const double COOLDOWN_COHETE = 3.0;//tiempo de cooldown en segs
const double COOLDOWN_BALA = 0.1;//tiempo de cooldown en segs

Musica musicaJuego("Musica/troll_song.wav");
Musica musicaJuego2("Musica/space_music.wav");

Texture t1, t2, t3, t4, t5, t6, t7, t8, t9, t10;


void funcionGameOver(RenderWindow* app)
{
	Texture gameover;
	if (!gameover.loadFromFile("images/gameover.jpg")) cout << "Se ha producido un error al cargar la imagen gameover" << endl;

	musicaJuego2.stop();

	Sprite Gameover(gameover);
	SoundBuffer gameOver;
	gameOver.loadFromFile("Musica/game_over.wav");
	Sound GameOver;
	GameOver.setBuffer(gameOver);
	GameOver.play();


	
	while (app->isOpen())
	{
		Event event;
		while (app->pollEvent(event))
		{
			if (event.type == Event::Closed)
				app->close();
		}

		app->clear();
		app->draw(Gameover);
		app->display();

	}

}

void funcionMostrar(RenderWindow* app)
{
	Font font;
	Text textos[5];
	if (!font.loadFromFile("Fonts/game.ttf")) {
		cout << "Ninguna fuente encontrada";
	}

	textos[0].setFont(font);
	textos[0].setFillColor(Color::Blue);
	textos[0].setString("Reglas de Juego");
	textos[0].setCharacterSize(70);
	textos[0].setPosition(100, 80);

	textos[1].setFont(font);
	textos[1].setFillColor(Color::White);
	textos[1].setString("Moverse --> Flechas arriba, izquierda, derecha");
	textos[1].setCharacterSize(50);
	textos[1].setPosition(300, 200);

	textos[2].setFont(font);
	textos[2].setFillColor(Color::White);
	textos[2].setString("Disparar laser --> Barra espaciadora");
	textos[2].setCharacterSize(50);
	textos[2].setPosition(300, 300);

	textos[3].setFont(font);
	textos[3].setFillColor(Color::White);
	textos[3].setString("Disparar cohete --> C");
	textos[3].setCharacterSize(50);
	textos[3].setPosition(300, 400);

	textos[4].setFont(font);
	textos[4].setFillColor(Color::White);
	textos[4].setString("Volver atras --> F1");
	textos[4].setCharacterSize(50);
	textos[4].setPosition(300, 500);



	while (app->isOpen()){

		Event event;
		while (app->pollEvent(event)) {
			if (event.type == Event::Closed){
				app->close();
			}
		}

		app->clear();
		for (int i = 0; i < 5; i++) {
			app->draw(textos[i]);
		}
		app->display();

	}

}



void cargarElementos() {
	t8.loadFromFile("images/uja.jpg");
	t1.loadFromFile("images/spaceship.png");
	t2.loadFromFile("images/background.jpg");
	t3.loadFromFile("images/explosions/type_C.png");
	t4.loadFromFile("images/rock.png");
	t5.loadFromFile("images/fire_blue.png");
	t6.loadFromFile("images/rock_small.png");
	t7.loadFromFile("images/explosions/type_B.png");
	t9.loadFromFile("images/rocket.png");

	t10.loadFromFile("images/3vidas.png");


	t1.setSmooth(true);
	t2.setSmooth(true);
}


class Animacion {
public:
	float Frame, speed;
	Sprite sprite;
	std::vector<IntRect> frames;

	Animacion() {}

	Animacion(Texture& t, float x, float y, float w, float h, int count, float Speed) {
		Frame = 0;
		speed = Speed;

		for (int i = 0; i < count; i++)
			frames.push_back(IntRect(x + i * w, y, w, h));//manipulacion poligonos 2d, la clase rect tiene 4 parámetros, ubicacion parte superior, ubicacion extremo izq, anchura y altura hacia abajo

		sprite.setTexture(t);
		sprite.setOrigin(w / 2, h / 2);
		sprite.setTextureRect(frames[0]);
	}


	void update() {
		Frame += speed;
		int n = frames.size();
		//std::cout << n << std::endl;
		if (Frame >= n) Frame -= n;
		if (n > 0) sprite.setTextureRect(frames[int(Frame)]);
	}

	bool isEnd() {
		return Frame + speed >= frames.size();
	}

};


class Entidad {
public:
	float x, y, dx, dy, R, angle;
	int life;
	std::string name;
	Animacion anim;

	Entidad()
	{
		life = 3;
		//std::cout << life<<std::endl;
	}

	void settings(Animacion& a, int X, int Y, float Angle = 0, int radius = 1)
	{
		anim = a;
		x = X; y = Y;
		angle = Angle;
		R = radius;
	}

	virtual void update() {};

	void draw(RenderWindow& app) {
		anim.sprite.setPosition(x, y);
		anim.sprite.setRotation(angle + 90);
		app.draw(anim.sprite);

		CircleShape circle(R);
		circle.setFillColor(Color(255, 0, 0, 170));
		circle.setPosition(x, y);
		circle.setOrigin(R, R);
		//app.draw(circle);
	}

	virtual ~Entidad() {};
};


class Asteroide : public Entidad {
public:
	Asteroide() {
		dx = rand() % 8 - 4;//a mayor dx mayor velocidad eje x
		dy = rand() % 8 - 4;
		name = "asteroid";
	}

	void update() {
		x += dx;
		y += dy;

		if (x > W) x = 0;  if (x < 0) x = W;
		if (y > H) y = 0;  if (y < 0) y = H;
	}

};

class decoracion : public Entidad {
public:
	decoracion() {
		name = "sprite";
		yacreado = false;
	}
	bool getCreado() {
		return yacreado;
	}
	void setCreado(bool estado) {
		yacreado = estado;
	}
private:
	bool yacreado;
};


class Bala : public Entidad {
public:
	Bala()
	{
		name = "bullet";
	}

	void  update()
	{
		dx = cos(angle * DEGTORAD) * 8;
		dy = sin(angle * DEGTORAD) * 8;
		
		x += dx;
		y += dy;

		if (x > W || x<0 || y>H || y < 0) life = 0;
	}

};

class Cohete : public Entidad {
public:
	Cohete()
	{
		name = "rocket";
	}

	void  update()
	{
		dx = cos(angle * DEGTORAD) * 6;
		dy = sin(angle * DEGTORAD) * 6;
		
		x += dx;
		y += dy;

		if (x > W || x<0 || y>H || y < 0) life = 0;
	}

};


class Jugador : public Entidad
{
private:
	int vidas;
	bool spriteVidasCreado = false;
public:
	
	bool thrust;

	Jugador() {
		name = "player";
		vidas = 3;
	}

	void update() {
		int maxSpeed = 10;
		if (thrust) {
			dx += cos(angle * DEGTORAD) * 0.2;//aceleracion
			dy += sin(angle * DEGTORAD) * 0.2;
		}
		else {
			dx *= 0.98; //inercia, a mayor valor pierde mas poco a poco la velocidad
			dy *= 0.98;
		}


		float speed = sqrt(dx * dx + dy * dy);
		if (speed > maxSpeed)
		{
			dx *= maxSpeed / speed;
			dy *= maxSpeed / speed;
		}
		x += dx;
		y += dy;

		if (x > W) x = 0; 
		if (x < 0) x = W;
		if (y > H) y = 0; 
		if (y < 0) y = H;
	}

	int getVidas() {
		return vidas;
	}

	void reduceVidas() {
		vidas--;
	}

	void setVidas(int num) {
		vidas = num;
	}
	void setSpriteVidasCreado() {
		spriteVidasCreado = true;
	}

	bool getSpriteVidasCreado() {
		return spriteVidasCreado;
	}
};


bool haChocado(Entidad* a, Entidad* b)
{
	return (b->x - a->x) * (b->x - a->x) +
		(b->y - a->y) * (b->y - a->y) <
		(a->R + b->R) * (a->R + b->R);
}







//////////////////////////////////////////////////

int main() {
	srand(time(0));

	RenderWindow menu(VideoMode(W, H), "Menu principal", Style::Default);
	MainMenu mainMenu(menu.getSize().x, menu.getSize().y);

	/*RenderWindow menuGameOver(VideoMode(W, H), "GameOver", Style::Default);
	GameOver gameOver(menuGameOver.getSize().x, menuGameOver.getSize().y);*/

	Texture gameOver;

	if (!gameOver.loadFromFile("images/gameover.jpg")) {
		cout << "error";
	}

	Sprite GameOver(gameOver);
	

	Musica musicaMenu("Musica/main_menu.wav");
	musicaMenu.cambiarVolumen(40);
	musicaMenu.reproducir();

	decoracion* dec3 = new decoracion();
	decoracion* dec2 = new decoracion();
	decoracion* dec = new decoracion();

	unsigned tiempo1=0;
	unsigned tiempo1b = 0;


	/////bucle del juego/////
	while (menu.isOpen()) {
		Event event;
		while (menu.pollEvent(event)) {
			musicaJuego2.stop();
			if (event.type == Event::Closed)
				menu.close();

			if (event.type == Event::KeyReleased) {
				if (event.key.code == Keyboard::Up) {
					mainMenu.MoveUp();
					break;
				}

				if (event.key.code == Keyboard::Down) {
					mainMenu.MoveDown();
					break;
				}

				if (event.key.code == Keyboard::Return) {
					RenderWindow Play(VideoMode(W, H), "JUEGO ASTEROIDES BY DANIEL CARRASCO DE LA CHICA");
					RenderWindow Options(VideoMode(W, H), "OPCIONES");
					RenderWindow About(VideoMode(W, H), "SOBRE EL JUEGO");
					
					cargarElementos();

					int x = mainMenu.MainMenuPressed();
					if (x == 0) {
						//paramos la anterior cancion
						musicaMenu.stop();

						//RenderWindow app(VideoMode(W, H), "Asteroides by Daniel");
						Play.setFramerateLimit(60);

						//CARGAMOS ELEMENTOS INICIALES


						//Sprite background(t8);
						Sprite background(t2);

						Animacion sExplosion(t3, 0, 0, 256, 256, 48, 0.5);//recortes imagenes
						Animacion sRock(t4, 0, 0, 64, 64, 16, 0.2); //16 frames de roca y a 0.2segs por frame
						Animacion sRock_small(t6, 0, 0, 64, 64, 16, 0.2);
						Animacion sBullet(t5, 0, 0, 32, 64, 16, 0.6);
						Animacion sPlayer(t1, 40, 0, 40, 40, 1, 0);
						Animacion sPlayer_go(t1, 40, 40, 40, 40, 1, 0);
						Animacion sExplosion_ship(t7, 0, 0, 192, 192, 64, 0.5);

						Animacion sCohete(t9, 0,0, 21.42857, 56, 7, 0.4);

						Animacion s3vida(t10, 0, 0, 150, 150, 1, 0.5);
						Animacion s2vida(t10, 0, 0, 96, 150, 1, 0.5);
						Animacion s1vida(t10, 0, 0, 54, 150, 1, 0.5);

						//animaciones de giro de la nave
						Animacion sPlayerLeft(t1, 0, 0, 40, 40, 1, 0);
						Animacion sPlayerRight(t1, 80, 0, 40, 40, 1, 0);
						Animacion sPlayer_goLeft(t1, 0, 40, 40, 40, 1, 0);
						Animacion sPlayer_goRight(t1, 80, 40, 40, 40, 1, 0);

						std::list<Entidad*> entities;

						for (int i = 0; i < NUM_ASTEROIDES_INICIALES; i++)
						{
							Asteroide* a = new Asteroide();
							a->settings(sRock, rand() % W, rand() % H, rand() % 360, 25);
							entities.push_back(a);
						}

						Jugador* p = new Jugador();
						p->settings(sPlayer, 200, 200, 0, 20); //animacion,x,y,angulo,radio
						entities.push_back(p);
						//cargamos sonidos y musica
						Sonido sonido("Sonidos/explosion.wav");
						Sonido sonidoexpnave("Sonidos/explosion_nave.wav");
						Sonido sonidoBala("Sonidos/disparo1.wav");
						Sonido sonidoCohete("Sonidos/cohete.wav");
						Sonido sonidoError("Sonidos/error.wav");
						
						sonidoBala.cambiarVolumen(30);
						sonidoCohete.cambiarVolumen(30);

						

						musicaJuego2.cambiarVolumen(100);
						musicaJuego2.reproducir();

						///DEJAMOS DE CARGAR
						while (Play.isOpen()) {
							Event aevent;
							while (Play.pollEvent(aevent)) {
								if (aevent.type == Event::Closed) {
									//p->setVidas(3);
									
									Play.close();
								}

								
								if (aevent.type == Event::KeyPressed) {
									if (aevent.key.code == Keyboard::F1) {
										dec3->setCreado(false);
										dec2->setCreado(false);
										dec->setCreado(false);
										dec3->life = 3;
										dec2->life = 3;
										p->setVidas(3);
										
										Play.close();
										musicaMenu.reproducir();
									}

									if (aevent.key.code == Keyboard::Space) {
										if (!balaUsada) {
											balaUsada = true;
											tiempo1b = clock();

											Bala* b = new Bala();
											b->settings(sBullet, p->x, p->y, p->angle, 10);
											entities.push_back(b);


											sonidoBala.reproducir();
										}
									}
									

									
									if (aevent.key.code == Keyboard::C) {
										if (!coheteUsado) {
											coheteUsado = true;
											tiempo1 = clock();

											
											sonidoCohete.reproducir();

											Cohete* r = new Cohete();
											r->settings(sCohete, p->x, p->y, p->angle, 10);
											entities.push_back(r);

											Cohete* r2 = new Cohete();
											float nuevoAngulo = p->angle - 180;
											//cout << "viejo: " << p->angle << endl;

											if (nuevoAngulo < 0) {
												nuevoAngulo = 180 + p->angle;

											}
											r2->settings(sCohete, p->x, p->y, 360 - p->angle, 10);
											entities.push_back(r2);

											Cohete* r3 = new Cohete();
											nuevoAngulo = p->angle + 90;
											if (nuevoAngulo > 360) {
												nuevoAngulo -= 360;
											}
											r3->settings(sCohete, p->x, p->y, nuevoAngulo, 10);
											entities.push_back(r3);

											Cohete* r4 = new Cohete();
											
											r4->settings(sCohete, p->x, p->y, rand(), 10);
											entities.push_back(r4);
										}
										else {
											sonidoError.reproducir();
										}

									}
									
								}
								//reseteo de cooldown
								unsigned tiempo2b = clock();
								if ((double(tiempo2b - tiempo1b) / CLOCKS_PER_SEC) >= COOLDOWN_BALA) {
									balaUsada = false;
								}
								
								unsigned tiempo2 = clock();
								if ((double(tiempo2 - tiempo1) / CLOCKS_PER_SEC) >= COOLDOWN_COHETE) {
									coheteUsado = false;
								}

							}


							
							//codigo juego
							
							if (Keyboard::isKeyPressed(Keyboard::Up)) {
								p->thrust = true;
							}
							else {
								p->thrust = false;
							}


							for (auto a : entities)
								for (auto b : entities) {
									if (a->name == "asteroid" && b->name == "bullet")
										if (haChocado(a, b)) {
											sonido.reproducir();
											a->life = 0;
											b->life = 0;

											Entidad* e = new Entidad();
											e->settings(sExplosion, a->x, a->y);
											e->name = "explosion";
											entities.push_back(e);


											for (int i = 0; i < 2; i++)
											{
												if (a->R == 15) continue;
												Entidad* e = new Asteroide();
												e->settings(sRock_small, a->x, a->y, rand() % 360, 15);
												entities.push_back(e);
											}

										}

									if (a->name == "asteroid" && b->name == "rocket")
										if (haChocado(a, b)) {
											sonido.reproducir();
											a->life = 0;
											b->life = 0;

											Entidad* e = new Entidad();
											e->settings(sExplosion, a->x, a->y);
											e->name = "explosion";
											entities.push_back(e);


											for (int i = 0; i < 2; i++)
											{
												if (a->R == 15) continue;
												Entidad* e = new Asteroide();
												e->settings(sRock_small, a->x, a->y, rand() % 360, 15);
												entities.push_back(e);
											}

										}

									if (a->name == "player" && b->name == "asteroid")
										if (haChocado(a, b)) {//asteroide ha chocado con jugador
											p->setVidas(p->getVidas()-1);
											//cout << "Vidas: " << p->getVidas()<<endl;
											sonidoexpnave.reproducir();

											b->life = 0;

											Entidad* e = new Entidad();
											e->settings(sExplosion_ship, a->x, a->y);
											e->name = "explosion";
											entities.push_back(e);

											p->settings(sPlayer, W / 2, H / 2, 0, 20);
											p->dx = 0;
											p->dy = 0;
										}
								}


							if (p->thrust) {
								p->anim = sPlayer_go;
								if (Keyboard::isKeyPressed(Keyboard::Right)) {
									//cout << "GIRANDO" << endl;
									p->anim = sPlayer_goRight;
									p->angle += 3; //girar mas brusco
								}
								if (Keyboard::isKeyPressed(Keyboard::Left)) {
									p->anim = sPlayer_goLeft;
									p->angle -= 3;
								}
							}
							else {
								p->anim = sPlayer;
								if (Keyboard::isKeyPressed(Keyboard::Right)) {
									//cout << "GIRANDO" << endl;
									p->anim = sPlayerRight;
									p->angle += 3; //girar mas brusco
								}
								if (Keyboard::isKeyPressed(Keyboard::Left)) {
									p->anim = sPlayerLeft;
									p->angle -= 3;
								}
							}




							for (auto e : entities) {
								if (e->name == "explosion") {
									if (e->anim.isEnd()) {
										e->life = 0;
									}
								}
							}

							if (rand() % TASA_GENERACION_ASTEROIDES == 0) //generacion nuevos asteorides
							{
								Asteroide* a = new Asteroide();
								a->settings(sRock, 0, rand() % H, rand() % 360, 25);
								entities.push_back(a);
							}

							//////dibujar//////
							
							//mostrar dibujo de vidas
							//funcionMostrar(&Play);
							if (!dec3->getCreado() && p->getVidas() == 3) {
								dec3->settings(s3vida, 70, 50, -90);//falta parametrizar
								entities.push_back(dec3);
								dec3->setCreado(true);
							}
							else if (!dec2->getCreado() && p->getVidas() == 2) {
								dec3->life = 0;
								dec2->settings(s2vida, 50, 50, -90);
								entities.push_back(dec2);
								dec2->setCreado(true);
							}
							else if (!dec->getCreado() && p->getVidas() == 1) {
								dec2->life = 0;
								dec->settings(s1vida, 30, 50, -90);
								entities.push_back(dec);
								dec->setCreado(true);
							}
							else if (p->getVidas() == 0) {
								dec3->life = 3;
								dec2->life = 3;
								funcionGameOver(&Play);	
							}
							//numero de entidades(memoria ocupada)
							//cout << "entis: " << entities.size() << endl;//para ver numero de entidades
							//cout << "Vidas: " << p->getVidas() << endl;
							for (auto i = entities.begin(); i != entities.end();)
							{
								Entidad* e = *i;

								e->update();
								e->anim.update();

								if (e->life == 0) { 
									i = entities.erase(i); 
									delete e; 
								}else {
									i++;
								}
							}

							

							Play.draw(background);

							for (auto i : entities) {
								i->draw(Play);
							}
							Play.display();
							Options.close();
							About.close();

						}
					}

					

					if (x == 1) {
						while (About.isOpen()) {
							Event aevent;
							Font font;
							Text textos[5];
							if (!font.loadFromFile("Fonts/game.ttf")) {
								cout << "Ninguna fuente encontrada";
							}

							while (About.pollEvent(aevent)) {
								funcionMostrar(&About);

								
								if (aevent.type == Event::Closed) {
									About.close();
								}

								if (aevent.type == Event::KeyPressed) {
									if (aevent.type == Event::KeyPressed) {
										if (aevent.key.code == Keyboard::F1) {
											About.close();
										}
									}
								}
							}
							Play.close();
							Options.clear();
							About.clear();
							
							About.display();
						}
					}

					if (x == 2) {
						menu.close();
					}
					break;
				}
			}
		}

		menu.clear();
		mainMenu.draw(menu);
		menu.display();

	}

	return 0;
}





