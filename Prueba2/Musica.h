#include <iostream>
#include <SFML/Audio.hpp>

using namespace sf;
using namespace std;
class Musica{
	public:
		Musica(std::string ruta);
		void reproducir();
		void pausar();
		void stop();
		void ponerEnBucle(bool bucle);
		void cambiarVolumen(int vol);
	private:
		Music music;
};


