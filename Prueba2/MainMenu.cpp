#include "MainMenu.h"

MainMenu::MainMenu(float ancho, float alto) {
	if (!font.loadFromFile("Fonts/game.ttf")) {
		cout << "Ninguna fuente encontrada";
	}
	
	mainMenu[0].setFont(font);
	mainMenu[0].setFillColor(Color::White);
	mainMenu[0].setString("Jugar");
	mainMenu[0].setCharacterSize(70);
	mainMenu[0].setPosition(500, 100);


	mainMenu[1].setFont(font);
	mainMenu[1].setFillColor(Color::White);
	mainMenu[1].setString("Informacion");
	mainMenu[1].setCharacterSize(70);
	mainMenu[1].setPosition(500, 200);

	mainMenu[2].setFont(font);
	mainMenu[2].setFillColor(Color::White);
	mainMenu[2].setString("Salir");
	mainMenu[2].setCharacterSize(70);
	mainMenu[2].setPosition(500, 300);

	MainMenuSelected = -1;

}

MainMenu::~MainMenu() {}

void MainMenu::draw(RenderWindow& window) {
	for (int i = 0; i < Max_main_menu; ++i) {
		window.draw(mainMenu[i]);
	}
}

void MainMenu::MoveUp() {
	if (MainMenuSelected - 1 >= 0) {
		mainMenu[MainMenuSelected].setFillColor(Color::White);


		MainMenuSelected--;
		if (MainMenuSelected == -1) {
			MainMenuSelected = 2;
		}
		mainMenu[MainMenuSelected].setFillColor(Color::Blue);
	}
	else {
		mainMenu[MainMenuSelected].setFillColor(Color::White);
		MainMenuSelected = Max_main_menu-1;
		mainMenu[MainMenuSelected].setFillColor(Color::Blue);
	}
}


void MainMenu::MoveDown() {
	if (MainMenuSelected + 1 <= Max_main_menu) {
		mainMenu[MainMenuSelected].setFillColor(Color::White);
		MainMenuSelected++;
		if (MainMenuSelected == Max_main_menu) {
			MainMenuSelected = 0;
		}
		mainMenu[MainMenuSelected].setFillColor(Color::Blue);
	}
}