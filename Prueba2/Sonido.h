#include <iostream>
#include <SFML/Audio.hpp>
#include <string>
#include <SFML/Audio/Sound.hpp>
#include <SFML/Audio/SoundBuffer.hpp>

using namespace sf;
class Sonido{
public:
	Sonido(std::string ruta);
	void reproducir();
	void cambiarVolumen(int vol);
private:
	Sound Sound;
	SoundBuffer Soundbuffer;
};

