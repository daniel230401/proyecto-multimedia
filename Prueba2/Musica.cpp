#include "Musica.h"
Musica::Musica(string ruta) {
    if (!music.openFromFile(ruta)) {
        std::cout << "Error al cargar musica";
    }
}

void Musica::cambiarVolumen(int vol) {
    music.setVolume(vol);
}

void Musica::pausar() {
    music.pause();
}

void Musica::ponerEnBucle(bool bucle) {
    music.setLoop(bucle);
}

void Musica::stop() {
    music.stop();
}

void Musica::reproducir() {
    music.play();
}