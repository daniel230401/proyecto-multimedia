#include "Sonido.h"
Sonido::Sonido(std::string ruta) {
   
    if (!Soundbuffer.loadFromFile(ruta)) {
        std::cout << "Error al cargar sonido";
    }

    Sound.setBuffer(Soundbuffer);
    
}

void Sonido::reproducir() {
    Sound.play();
}

void Sonido::cambiarVolumen(int vol) {
    Sound.setVolume(vol);
}
